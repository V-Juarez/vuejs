<h1>Vue.js 2</h1>

<h3>Ignacio Anaya</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Qué aprenderás sobre Vue.js 2](#qué-aprenderás-sobre-vuejs-2)
  - [El Framework Progresivo](#el-framework-progresivo)
  - [¡Hola Vue!](#hola-vue)
- [2. Rendering Declarativo y Manipulación de DOM](#2-rendering-declarativo-y-manipulación-de-dom)
  - [Introducción al Rendering Declarativo](#introducción-al-rendering-declarativo)
  - [Expresiones y Propiedades](#expresiones-y-propiedades)
  - [Atributos Dinámicos](#atributos-dinámicos)
  - [Control de Flujo con Directivas](#control-de-flujo-con-directivas)
  - [Renderizado de Listas](#renderizado-de-listas)
  - [Manejo de Eventos](#manejo-de-eventos)
  - [Clases en tiempo real](#clases-en-tiempo-real)
  - [Estilos en tiempo real](#estilos-en-tiempo-real)
  - [Computed Properties y Watchers](#computed-properties-y-watchers)
  - [Two-Way Data Binding](#two-way-data-binding)
  - [Ejercicios de práctica](#ejercicios-de-práctica)
- [3. Sistema de Componentes](#3-sistema-de-componentes)
  - [Sistema de Componentes](#sistema-de-componentes)
  - [Crear Componentes Custom](#crear-componentes-custom)
  - [Comunicación entre Componentes: propiedades](#comunicación-entre-componentes-propiedades)
  - [Comunicación entre Componentes: eventos](#comunicación-entre-componentes-eventos)
  - [Slots](#slots)
  - [Ciclo de Vida y Hooks](#ciclo-de-vida-y-hooks)
  - [Antes de continuar, ¡un repaso!](#antes-de-continuar-un-repaso)
  - [Ejercicios de práctica](#ejercicios-de-práctica-1)
- [4. Ambiente de Desarrollo](#4-ambiente-de-desarrollo)
  - [VS Code + Vetur / Chrome / Firefox + Dev Tools](#vs-code--vetur--chrome--firefox--dev-tools)
  - [Qué es, cómo usarlo y aplicaciones profesionales con el CLI](#qué-es-cómo-usarlo-y-aplicaciones-profesionales-con-el-cli)
  - [Single File Components](#single-file-components)
  - [Funcionalidades, UI y comandos básicos](#funcionalidades-ui-y-comandos-básicos)
- [5. Platzi Exchange](#5-platzi-exchange)
  - [Introducción y Setup de Tailwind CSS](#introducción-y-setup-de-tailwind-css)
  - [Primeros Componentes](#primeros-componentes)
  - [Introducción y Setup de Vue Router](#introducción-y-setup-de-vue-router)
  - [Introducción a Fetch y API de Coincap](#introducción-a-fetch-y-api-de-coincap)
  - [Mejorar la UI con filtros](#mejorar-la-ui-con-filtros)
  - [Rutas dinámicas](#rutas-dinámicas)
  - [Navegación Programática](#navegación-programática)
  - [Utilizar componentes de terceros](#utilizar-componentes-de-terceros)
  - [Problemas de Reactividad](#problemas-de-reactividad)
  - [Mejorar proyecto: filtros y ordenar](#mejorar-proyecto-filtros-y-ordenar)
  - [Mejorar proyecto: links y conversión](#mejorar-proyecto-links-y-conversión)
- [6. ¡A producción!](#6-a-producción)
  - [Despliegue de la app a Netlify](#despliegue-de-la-app-a-netlify)
  - [Rendering Declarativo y Manipulacion de DOM](#rendering-declarativo-y-manipulacion-de-dom)

# 1. Introducción

## Qué aprenderás sobre Vue.js 2

Si te haces la pregunta de tomar este curso o directamente el de la versión 3, te recomiendo que si lo tomes ya que aprenderás las bases fundamentales para manejar y entender las versiones posteriores a esta.

**Emocionante** documental donde podrás conocer un poco sobre la historia de Vue.js, su creador y otros aspectos que te interesaran.

[Vue.js: El Documental](https://www.youtube.com/watch?v=OrxmtDw4pVI) 😃

## El Framework Progresivo

¿Qué es Vue.js?

Vue (pronunciado /vjuː/ en inglés, como view) es un framework progresivo para construir interfaces de usuario. A diferencia de otros frameworks monolíticos, Vue está diseñado desde el inicio para ser adoptado incrementalmente. La biblioteca principal se enfoca solo en la capa de la vista, y es muy simple de utilizar e integrar con otros proyectos o bibliotecas existentes. Por otro lado, Vue también es perfectamente capaz de soportar aplicaciones sofisticadas de una sola página (en inglés single-page-application o SPA) cuando se utiliza en combinación con herramientas modernas y librerías compatibles.
[Mas información](https://es-vuejs.github.io/vuejs.org/v2/guide/)

Vue es un framework muy versátil, escalable puede utilizarse para cosas muy sencillas como para cosas muy complejas.

Es un framework muy performante, tiene un tamaño muy pequeño y esta pensado para trabajar con el DOM. Puede usarse para crear una landing page o una Single Page Application.

**VUE**=> Es una librería enfocada en la vista, utiliza el virtualDOM y es totalmente reactiva y tiene 2 características principales: SISTEMA DECLARATIVO O DECLARATIVO RENDER(Es una de las funcionalidades que nos permiten a nosotros de forma imperativa poder manejar el DOM e interactuar con el mismo).

y SISTEMA DE COMPONENTES (Es lo que hace que podemos modularizar nuestra app en diferentes tipos de componentes reutilizables o no que nos permitan tener un código semántico y mucho más fácil de mantener y de leer).

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org//images/icons/android-icon-192x192.png)Vue.js](https://vuejs.org/)

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)vuejs · GitHub](https://github.com/vuejs)

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)GitHub - vuejs/vue: 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.](https://github.com/vuejs/vue)

[![img](https://www.google.com/s2/favicons?domain=https://static.npmjs.com/b0f1a8318363185cc2ea6a40ac23eeb2.png)vue - npm](https://www.npmjs.com/package/vue)

## ¡Hola Vue!

Tenemos que envolver nuestro html en un selector que luego vamos a utilizar

```html
<div id="app">
  <h2>{{ title }}</h2>
</div>
```

Para crear una app la inicalizamos creando una `new Vue` teniendo como argumento un objeto con el elemento `el` que es el elemento html al que hace referencia y la `data` que es la información que puede utilizar.

```javascript
const app = new Vue({
  el:'#app',
  data () {
      return {title:'Hola Vue'}
  },
})
```

Para este caso nuestra aplicación regresará un:

## Hola Vue

Pero resulta que creamos una variable con nuestra app, y le podemos cambiar el valor:

```javascript
app.title = "Adios Vue"
```

Si hacemos ésto nos cambiaría el contenido html al valor `Adios Vue`

```vue
Adiós Vue
```



> Reactividad hace referencia a la capacidad de detectar cambios en tiempo real para actualizar estados

**el:** Es un pseudo-selector de CSS que nos permite con un selector obtener un elemento dentro del dom.

**data:** una function que devuelve un objeto, dentro del el vamos a definir todas las propiedades que usaremos dentro de nuestro template, si queremos usar una variable title dentro de nuestra vista tenemos que definirla acá.

Para crear una app en Vue.js primero tenemos que definir su instancia con new Vue, es importante porque representa nuestra aplicación, recibe un objeto y dentro de el escribimos las propiedades y configuraciones que la app puede tomar.

“el:” es un pseudo selector de CSS para tomar un elemento del DOM.

Todas las apps de vue tienen un único elemento padre y todos los elementos hijos pertenecen a la app de vue, todo lo que este fuera estará fuera de la app.

“data” función que devuelve objeto, dentro de este vamos a definir todas las propiedades.

Las expresiones son la manera que nos da vue de crear templates de manera dinámica dentro de nuestro HTML

[![img](https://www.google.com/s2/favicons?domain=https://static.codepen.io/assets/social/facebook-default-05cf522ae1d4c215ae0f09d866d97413a2204b6c9339c6e7a1b96ab1d4a7340f.png)codepen](https://codepen.io/)

# 2. Rendering Declarativo y Manipulación de DOM

## Introducción al Rendering Declarativo

El Declarative Rendering es la opción que nos propone VueJS para interactuar con el DOM, principal con el concepto de Two Way Data Binding, esto quiere decir que vamos a tener:

- Vista: aquí tenemos el HTML.
- Estado: aquí tenemos nuestros datos de JS.
- Usuario: es el que introduce cambios en la vista.

La vista se encarga de decirle al estado que hay cambios, a lo cual el estado va a reaccionar y mandar una nueva vista.

El estado se encarga de decirle a la vista como y cuando tiene que compilar y cual es el resultado que se tiene que lograr, esto lo hace con una función que se llama render.

### Two way data binding

Two-way data binding es un patrón MVVM (model -> view -> view -> model) donde se enlazan 2 elementos en 2 direcciones (cuando cambia uno cambia el otro)

Sirve para mantener sincronizados los datos con el DOM sin esfuerzos adicionales

Two-way data binging y en vue pasa de la siguiente manera:

El estado comunica a la vista qué es lo que va a modificar y la vista (donde el usuario interactúa con la app) responde con los cambios provocados por el usuario para que el estado vuelva a empezar con el proceso.

## Expresiones y Propiedades

```vue
<div id='app'>
  <h1>{{ title }} </h1>
  <p>{{ 1 + 1 + 1 }}</p>
  <p>{{ true || false }}</p>
   <p>{{ true ? true : false }}</p>
   <p>{{ 'string'.toUpperCase() }}</p>
   <p>{{ JSON.stringify({ name: 'Nacho' }) }}</p>
</div>
```

> Casi cualquier tipo de código JS puede ser utilizado dentro de una expresión. Hay cosas que no podríamos hacer: un if, un else, un while, un switch, un for…

### Template synax


Vue.js utiliza una sintaxis de plantilla basada en HTML (Template Syntax) que le permite vincular declarativamente el DOM representado a los datos de la instancia de Vue.

Todas las plantillas de Vue.js son HTML válidos que pueden ser analizados por navegadores y analizadores HTML que cumplan con las especificaciones.

Por lo tanto podemos utilizar todas las funcionalidades de javascript que tengan un retorno explícito, tales como **variables**, **progrmación funcional**, **expresiones lógicas**, **llamadas de funciones**, **if ternarios**, **métodos** y **atributos** de objetos, etc, pero no podemos hacer uso de if, else, while porque éstos no tienen un retorno.

### Expresiones y Propiedades

Dentro de una expresión {{ … }} podemos escribir casi cualquier código js válido.
Ejemplo: operaciones aritmeticas *(suma, resta, multi, división)*, expresiones lógicas, llamar funciones, etc. Las sentencias condiciones no se pueden usar dentro de ellas *(por ejemplo if and else, for, while, etc.)*, ya que Vue tiene su propia forma de ejecutarlas atreves de algo llamado *directivas*

## Atributos Dinámicos

El shorthand para `v-bind` es :
Podemos user el shorthand así:

```vue
<img :src="img" :alt="name">
```

### [Binding de Atributos html](https://es.vuejs.org/v2/guide/class-and-style.html)

Los atributos en html son las propiedades que puede tener una etiqueta, mismos que se pueden vincular (bind) a variables javascript con directivas.

Las directivas nos permiten vincular el dom con javascript de manera declarativa, para hacer un data binding podemos utilizar la directiva **`v-bind:`atributo**=“valor” vinculando el atributo a nuestro objeto de vue, creando en `data()`, recordando que dentro del valor podemos poner código js.

- Las directivas son las herramientos que nos da Vue para poder manipular el DOM.
- Ejemplo: v-if, v-show, v-bind, v-for

Las directivas es la herramienta que nos ofrece Vue para poder manipular el DOM y para poder manipularlo de forma declarativa.
Es decir, que podemos utilizar diferentes tags dentro de nuestros elementos HTML.

Las directivas de Vue siempre se escriben con v corta, guión y el nombre de la directiva.0
Ejemplo: v-bind:src=“img” v-bind:alt="name"
Así podemos definir valores dinámicos para estos atributos.

[![img](https://www.google.com/s2/favicons?domain=https://www.postgresql.org/docs/9.2/plpgsql.html/favicon.ico)BTC - Logo](https://cryptologos.cc/logos/bitcoin-btc-logo.png)

## Control de Flujo con Directivas

La principal diferencia entre v-if y v-show es que v-if renderiza el elemento en el DOM solo si se cumple la expresión y v-show renderiza todos los elementos en el DOM y luego utiliza la propiedad display de CSS para ocultarlo si no cumple con la expresión.
v-show se utiliza preferentemente si el elemento cambia frecuentemente y v-if cuando no cambia a lo largo del tiempo.

• v-bind: Enlaza dinámicamente uno o mas atributos del elemento. Se puede abreviar así “ :src = “img”.
• v-if: De acuerdo con el cumplimiento de la condición construye o destruye el elemento.
• v-else-if: Funciona de la misma manera que “v-if”, pero para usarla debe existir previamente la directiva “v-if” o “v-else-if”.
• v-else: Para usar esta directiva previamente debe existir la directiva “v-if” o “v-else-if”.
• v-show: Dependiendo de la condición, cambia la propiedad “display” del atributo style del elemento.

Emojis en Windows 10 => tecla windows + Tecla punto (.) o Tecla coma (,).
Mas información: [https://es.vuejs.org/v2/api/#Directivas](https://platzi.com/clases/1752-vuejs2/24520-control-de-flujo-con-directivas/url)

## Renderizado de Listas

Se utiliza la directiva `v-for` para recorrer el arreglo.
`v-for = “p in prices”`

Al utilizar esta directiva se recomienda el uso de key, ya que permite identificar de manera unívoca a cada elemento. Esto permite que si la lista se reordene, modifique o actualice la lista pueda seguir identificando cual es el elemento.

Podemos llamar al indice del elemento de la lista:
`v-for="(p,i) in prices"`

```js
// listas 
prices: [8400, 7900, 8200, 9000, 9400, 10000, 10200]
v-for = " (p, i) …"

pricesWithDays: [
                { day: 'Lunes', value: 8400 },
                { day: 'Martes', value: 7900 },
                { day: 'Miercoles', value: 8200 },
                { day: 'Jueves', value: 9000 },
                { day: 'Viernes', value: 9400 },
                { day: 'Sabado', value: 10000 },
                { day: 'Domingo', value: 10200 },
            ]

// p hace referencia al elemento en sí
// i al índice del elemento.
```

Directiva v-for: es bastante común ya que nos permite renderizar arrays o listas dentro de nuestro html, es decir, nos permite repetir un set de código HTML en base a los elementos que podamos tener un array.

Cuando usamos un v-for se aconseja usar la propiedad key que le permite a Vue identificar de manera inequívoca a cada uno de los elementos, nos permite en caso de que la lista se reordene, modifique o actualice, Vue pueda seguir detectando cual es elemento y no pierda el track de esas dependencias.

También se puede acceder al elemento índice que representa cada uno de los elementos dentro del array usando como ejemplo: “(p, i) in prices”, donde el primer valor representa al elemento en si, y el 2 al índice de ese elemento.

Un v-for puede mostrar más allá de números o arrays simples, también puede haber un array de objetos

## Manejo de Eventos

Abreviar v-on con un simple **@**

```html
<span v-on:click="toggleShowPrices">{{showPrices ? '🙈' : '🙊'}}</span>
```

Abreviado

```html
<span @click="toggleShowPrices">{{showPrices ? '🙈' : '🙊'}}</span>
```

Dejo un enlace a la documentacion de vuejs donde podemos leer mas acerca de la sintaxis del template
[docs](https://es.vuejs.org/v2/guide/syntax.html#Modo-abreviado)

![Captura de Pantalla 2020-03-30 a la(s) 6.40.14.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202020-03-30%20a%20la%28s%29%206.40.14-21ca5df0-2194-4ec2-9471-a6f7f701660b.jpg)

methods.
Es una propiedad de la instancia de Vue. Es un objeto donde se pueden definir funciones que pueden ser disparados por acciones en la vista.

Para acceder a propiedades de otro scope se utiliza la keyword `this` seguido de un punto y el nombre de la propiedad.

```js
methods:{
   toggleShowPrices(){
   	this.showPrices = !this.showPrices;
 	}
}
```


Por último agregar la directiva `v-on` ante determinado evento, en el HTML que permite disparar el método.

```html
<span v-on:click="toggleShowPrices">Ver precios</span>
```

Otra directiva que se puede utilizar en v-on, es mouseover:

```html
v-on:mouseover="toggleShowPrices"   
v-on:mouseout="toggleShowPrices"
```

## Clases en tiempo real

En esta clase se mencionan los **Operadores Ternarios**:
Las operaciones ternarias, o “*nested ternaries*” en inglés, son expresiones que se utilizan en JavaScript para las condicionales
`if... else...`

Una expresión en JavaScript como por ejemplo:
`if changepercent > 0 { show: 'green' } else { show: 'red' }`

Es lo mismo que la siguiente operación ternaria:
`changePercent > 0 ? 'green' : 'red'`

Para más información sobre el operador, puedes ver [la documentación de Mozilla para developers sobre Operadores condicionales ternarios](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/Conditional_Operator)

Modificar el atributo class de una etiqueta usando la directiva v-bind: usando operador ternario, o validando con objetos.

```js
<li
	v-bind:class="{orange: p.price === price, red: p.price < price, green: p.price > price}"
        v-for="(p, i) in pricesWithDay" 
        v-bind:key="p.day">
        {{ i }} - {{ p.day }} - {{ p.price }}
</li>
```

> El uso de **`v-bind:class`** es completamente combinable con **`class`**, por lo tanto puedes tener un v-bind:class junto con una clase y no genera error.

## Estilos en tiempo real

> ​	Para bindear alguna propiedad, como en este caso usamos **v-bind:class** podemos utilizar el atajo **:**, por lo que si queremos bindear la clase podemos utilizar el atajo como: **:class**, ambas maneras son válidas dentro de Vue.js
>
> > Funciones **split()**, **reverse()** y **join()**. Interesante avanzar con las directivas de **Vue** y a la vez conocer estas “cosillas” de **javaScript**.

Se pueden aplicar style directamente a un elemento en el DOM, mediante un objeto:

```js
v-bind:style="{background: '#'+color}"
```

## Computed Properties y Watchers

**Propiedades computadas (Computed) =** propiedades que se calculan en tiempo real en base a los valores de otras propiedades.

**Watcher =** Funciones que ejecutan un código es decir que por medio de un cambio de la observación de una variable se puede disparar determinado código o ejecutar una función.
Podemos pensarlo como **un Disparador de código.**

- Las **propiedades computadas** son funciones que nos devolverán un valor en tiempo real basados en los **valores de las propiedades**, es decir en aquellas que están definidas en nuestro **data**.

```js
computed: {
      title() {
         return `${this.name} - ${this.symbol}`
      }
   },
```

- Los **watchers** son funciones que determinan un código a partir de la observación del cambio en una **variable**.
  El nombre de un watcher, es decir la función dentro del **watch** debe corresponder con el nombre de la **propiedad** en data.

```js
watch: {
      showPrices (newValue, oldValue) {
         console.log(newValue,oldValue);
      }
   },
```

## Two-Way Data Binding

La directiva `v-model` permite linkear las cosas que puede escribir el usuario mediante un input con las propiedades que tenemos definidas en data. Es lo que permite que cada vez que se cambia la vista a través de interacciones con el usuario se refesque el código y cada vez que se refresque el código también se actualice la vista, esto se le conoce como Two-Way Data Binding.
.
En index.html:

```
<input type="number" v-model="value">
<span>{{ converterdValue }}</span>
```

En app.js:
Dentro de data se agrega la propiedad value y se inicializa en 0. Y dentro de computed porque se opera con dos propiedades de data se agrega esta función para que retorne el valor de la operación.

```js
converterdValue(){
      if(!this.value){
        return 0
      }
      return this.value/this.price
    }
```

> En resumen, v-model permite habailitar el TwoWay Data Binding, que es prácticamente que, cada vez que se escirba algo en un input, ese valor será actualizado automáticamente en la variable, y si la variable llegara a cambiar dentro del código, en el input también se actualizaría

## Ejercicios de práctica

Haz fork de [este](https://codepen.io/ianaya89/pen/YzzOEjK) CodePen para obtener la estructura basica del proyecto:

### Consideraciones:

- La aplicación debe cumplir los siguientes lineamientos:
  - Un propiedad `courses` que sea un array y permita almacenar la lista de cursos.
  - Tener una propiedad `title` y otra `time` que se usean para agregar un nuevo curso a la lista, estas propiedades deben estar enlazados a los inputs usando `v-model`.
  - Un boton con un metodo `addCourse` (enlazado con `v-on`) que permita agregar un nuevo courso usando los valores de `title` y `time`.
  - Una propiedad computada `totalTime` que recorra toda la lista de cursos y retorne la suma del tiempo invertido en educacion.
  - Mostrar la lista de cursos tomados, con el titulo de los mismos y las horas de cada uno usando `v-for`.
  - Mostrar el total de horas con `totalTime`, en caso que no existan cursos se debe mostrar un mensaje indicandolo.

#### [Solución](https://codepen.io/ianaya89/pen/rNNZYLY)

[**Repositorio**](https://github.com/daniektj/Reto-01-Curso-Basico-de-Vue.js)
[**Link del Proyecto:**](https://daniektj.github.io/Reto-01-Curso-Basico-de-Vue.js/)

![screenshot.png](https://static.platzi.com/media/user_upload/screenshot-0a36c41a-8295-4c6f-a3bb-5a6a6355b5f8.jpg)

# 3. Sistema de Componentes

## Sistema de Componentes

Los componentes son la segunda funcionalidad importante que tiene Vue. Se basan en las especificaciones de web components APIs. Permiten modularizar mi aplicación en diferentes pedazos de htm, javascript y Css para tener un código más legible y semántico. Es decir, cada componente puede ser utilizado a lo largo del proyecto, tiene todo lo que necesita para existir, tiene su lógica, tiene diseño y tiene estructura.

Cuando trabajamos con Html y el DOM siempre tenemos una estructura de árbol, es decir, una estructura jerárquica DOM. Tenemos un componente principal (en este caso, el recuadro azul en la imagen abajo) y luego tenemos componentes hijos que representan contenedores con diferente tipo de contenido. Además, podemos tener otros componentes hijos, es decir, cada uno también tiene sus propios elementos Html.

![Captura de pantalla (6).png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20%286%29-d12bf9b3-5cac-40c2-afc6-0f66581c7ab0.jpg)

![Captura de pantalla (7).png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%20%287%29-87169421-6045-4911-9d1b-09931d91e621.jpg)

Entonces podemos decir que la estructura al ser de árbol, siempre tiene un componente principal y componentes hijos que salen de ese componente principal. Este componente principal estaba representado por lo que conocemos como el componente root, o nuestra aplicación (App, en este caso) y luego tenemos los componentes hijos, como puede ser un header , un footer y después diferente tipo de contenido que podemos tener a lo largo nuestra estructura. La idea de los componentes es llevar toda nuestra lógica a pedazos de Html reutilizables que luego vamos a escribir de manera muy sencilla y semántica dentro de nuestro elemento o dentro de nuestro componente principal.

Por ejemplo, podríamos pensar un componente que tenga un header de Html, que también tenga algo así como un componente que se llame login . De igual modo otro componente que podría ser Custom y luego tener un footer. De esta forma combinamos los elementos de Html5 con nuestros propios componentes para conseguir un código semántico y además tenemos la ventaja de que dentro de otra página o dentro de otro componente, también podemos utilizar los componentes que creamos, por ejemplo, el componente Custom o el componente footer que es nativo de Html.

Cuando se trabaja con HTML y el DOM siempre se tiene una estructura de árbol (jerárquica) donde hay un componente principal e hijos que pueden tener otros componentes hijos.

El componente principal está representado por lo que se conoce como el componente root o nuestra app, y luego le siguen los hijos.

La idea de los componentes es llevar toda nuestra lógica a pedazos de HTML reutilizable que luego se escriben de manera sencilla y semántica dentro de nuestro componente principal.

## Crear Componentes Custom

Para crear un componente se utiliza la propiedad component de Vue, la cual tiene casi los mismos metodos que la instancia de Vue donde veniamos haciendo todo, nada mas que este tiene el metodo template donde va a ir el “html” de nuestro componente 😄

`js`

```js
Vue.component('counter', {
  data () {
    return {
      counter: 0
    }
  },
  
  methods: {
    increment() {
      this.counter += 1
    }
  },
  
  template: `
    <div>
     <button v-on:click="increment">Click me!</button>
     <span>{{ counter }}</span>
    </div>
  `
})
​
new Vue({
  el: '#app',
  
  data () {
    return {
      title: 'Hola'
    }
  }
})
```

`html`

```html
<div id="app">
  <h1>{{ title }}</h1>
  <counter></counter>
</div>para crear un componente se utiliza la propiedad component de Vue, la cual tiene casi los mismos metodos que la instancia de Vue donde veniamos haciendo todo, nada mas que este tiene el metodo template donde va a ir el “html” de nuestro componente 😄
```

`Vue.component()`

Es la función que permite registrar un componente nuevo y asignarle un nombre. Recibe dos parámetros, el primero es el nombre y el segundo es un objeto donde tenemos las mismas propiedades (data, computed, etc) que tenemos en la instancia de Vue.

Con la propiedad template, podemos definir el HTML que va a tener el componente en formato de string.

Dentro de un componente comenzamos a tener la estructura:

- data
- methods
- template

> vUn componente es una etiqueta custom que creamos nosotros para hacer alguna sección que funcione por su propia cuenta y eso se logra a través del método Vue.component

[![img](https://www.google.com/s2/favicons?domain=https://www.postgresql.org/docs/9.2/plpgsql.html/favicon.ico)Codepen](https://codepen.io/ianaya89/pen/KKPOdzO)

## Comunicación entre Componentes: propiedades

> 🐪 camelCase
> 🧮 PascalCase
> 🐍 snake_case
> 🍢 kebab-case

En props definimos las propiedades que el componente padre le va a enviar al hijo. Los props se utilizan igual que las propiedades de data, se utilizan a través de this. El componente hijo no puede escribir o modificar los props que le mande el padre.

El nombre del componente se escribe en Pascal Case, donde cada palabra inicia con la primera letra en mayúscula. Al momento de llamar al componente en el HTML, Vue parsea el nombre a Kebab Case, una nomenclatura con guiones lo cual permite mantener una consistencia con los tags HTML.

Se recomienda mantener la consistencia entre los lenguajes, utilizar Pascal Case dentro de JavaScript: `CoinDetail` ; y Kebab Case dentro de HTML: `coin-detail`.

Para pasarle las propiedades al componente se hace desde el HTML usando la directiva v-bind: v-bind:change-percent = “changePercent”. Aquí también aplica lo del Kebab Case.

Los componentes de Vue necesitan tener un componente padre único que encierre a todos los demás.

Cuando las propiedades tienen un elemento central común es más cómodo trabajarlas mediantes un objeto, así evitamos tener que estar pasando tantos props.

Las propiedades, pueden ser listadas como un objeto donde se refleja los nombres y valores/tipos:

```js
props: {
  title: String,
  likes: Number,
  isPublished: Boolean,
  commentIds: Array,
  author: Object,
  callback: Function,
  contactsPromise: Promise // or any other constructor
}
```

- Componente hijos no puede modificar las propiedades enviadas por el padre.
- Es necesario usar v-bind para pasar propiedades dinámicas de lo contrario se pasara el string literal.
- Si se definen las propiedades en el componente en camelCase se podrán citar en el padra así msimo o en formate de guiones bajos.

codigo

```js
Vue.component('CoinDetail',{
    props:['coin'],
    data(){
        return{
            showPrices:false,
            value:0
        }
    },
    computed:{
        title(){
            return `${this.coin.name} + ${this.coin.symbol}`;
        },
        convertedValue(){
            if(!this.value){
                return 0;
            }
            return this.value / this.coin.price
        }
    },
    methods:{
        toggleShowPrices(){
            this.showPrices =!this.showPrices
        }
    },
    template: `
    <div>
        <img 
            :src="coin.img" alt="" 
            @mouseover="toggleShowPrices"
            @mouseout="toggleShowPrices"
        >
        <h1 :class="coin.changePercent > 0 ? 'green' : 'red' ">
            {{title}}
            <span v-if="coin.changePercent > 0">👍</span>
            <span v-else-if="coin.changePercent < 0">👎</span>
            <span v-else>🤞</span>
            <span @click="toggleShowPrices">
                {{showPrices ? '🙈' : '🐵'}}
            </span>
            <input type="number" v-model="value">
            <span>{{convertedValue}}</span>
            <ul v-show="showPrices">
                <li
                class="uppercase"
                :class="{orange: item.value == coin.price, red: item.value < coin.price, green: item.value > coin.price }"
                v-for="(item, index) in coin.pricesWithDays"
                :key="item.day">
                    {{index}} - {{item.day}} - {{item.value}}
                </li>
            </ul>
        </h1>
    </div>
    `
});

new Vue({
    el:'#app',
    data() {
        return {
            btc:{
                name:'Bitcoin',
                symbol:'BTC',
                img:'https://cryptologos.cc/logos/bitcoin-btc-logo.png',
                changePercent:-10,
                price:8400,
                pricesWithDays: [
                    { day: 'Lunes', value: 8400 },
                    { day: 'Martes', value: 7900 },
                    { day: 'Miercoles', value: 8200 },
                    { day: 'Jueves', value: 9000 },
                    { day: 'Viernes', value: 9400 },
                    { day: 'Sabado', value: 10000 },
                    { day: 'Domingo', value: 10200 },
                ],
            },
            
            color: 'f4f4f4'
        }
    },/*
    methods: {
        toggleShowPrices(){
            this.showPrices = !this.showPrices;
            this.color =this.color.split('')
            .reverse().join('');
        }
    },
    */
})
```

## Comunicación entre Componentes: eventos

Cuando el componente hijo debe enviar información al componente padre, se deben enviar eventos, ya que las propiedades del componente padre nunca deben ser actualizadas por el componente hijo.

La comunicación de padres hacia hijos es a través de propiedades y la comunicación entre hijos hacia padres es a través de eventos.

`v-on` para enviar eventos hacia el componente padre.

$emit pertenece al core de VueJS permite indicar que el componente debe de emitir un evento.

```vue
this.$emit('nombre-del-evento')
```

En el HTML dentro de nuestro componente hijo agregamos la directiva v-on para indicar que esté escuchando el evento:

```vue
<coin-detail v-on:nombre-del-evento="funcion-a-ejecutar"</coin-detail>
```

Cada componente es totalmente aislado, es por ello que es importante que la función data sea una función que devuelva un objeto, de esta forma podemos garantizar que cada componente tiene su propio estado y que cuando se modifica este estado no se va a modificar el estado de otra instancia de este mismo componente. Si no fuera una función no seriamos capaces de mantener una referencia aislada de cada uno de nuestros componentes.

COMUNICACIÓN ENTRE COMPONENTES: EVENTOS (Clase 18)

¿Qué debemos hacer cuando un componente hijo necesita enviar información a un componente padre? Lo que debemos hacer es enviar eventos. Las propiedades del componente padre nunca deben ser actualizadas por el componente hijo. En caso de que haya que modificar una de estas propiedades, el componente hijo tiene que notificar al padre y esta notificación se hace a través de eventos. Se puede decir entonces que la comunicación de padre a hijo es con propiedades y de hijos a padres es con eventos. De esta manera mantenemos la consistencia en Vue.js.

Recordemos que usamos el v-bind para modificar en tiempo real o tener un atributo dinámico en cuanto a las propiedades y vamos a usar la emisión de eventos con la directiva v-on para que el componente hijo pueda enviar información al componente padre.

Tomando el código de la clase precedente, lo que vamos a hacer ahora es modificar la variable color del componente padre usando el componente hijo. El componente hijo debe emitir un evento para avisarle al componente padre.

Vamos a hacerlo con la función this.$emit(‘change-color’) inserta en la función toggleShowPrices() que está en methods del componente hijo.

```js
methods: {
    toggleShowPrices() {
      this.showPrices = !this.showPrices;
      this.$emit('change-color')
     }
  },
```

Luego, en el Html , en <coin-detai> insertamos la directiva v-on: change-color=”updateColor ” (Significa: escuchar al evento change-color)

```js
coin-detail 
    v-on:change-color="updateColor"
    v-bind:coin="btc"
    >  </coin-detail>
```

Ahora, en el componente principal (el padre) se define, en este ejemplo, el comportamiento de change-color, así:

```js
methods:{
updateColor(){
  this.color = this.color.split('').reverse().join('')
    }
  }
})
```

Al momento de emitir un evento también podemos emitir un valor que vaya acompañando ese evento. En el argumento de la función this.$emit(‘change-color’) colocaremos el color que queremos, asi:

```js
methods: {
    toggleShowPrices() {
      this.showPrices = !this.showPrices;
      this.$emit('change-color', 'ff96C8')
     }
  },
```

Luego en el componente principal podríamos cambiar el método updateColor() así:

```js
methods:{
updateColor(color){
  this.color = color || this.color
  .split('').reverse().join('')
     }
   }
})
```

De esta manera el color va a ser siempre ‘ff96C8’ (color rosa). Una variante que podríamos hacer es esta:

```js
methods: {
    toggleShowPrices() {
      this.showPrices = !this.showPrices;
      this.$emit('change-color', 
      this.showPrices ? 'ff96C8' : '3d3d3d')
     }
  },
```

Ahora bien, si repito el componente `<coin-detail>` de la forma como se ve enseguida, cada `<coin-detail>` se va a comportar de manera aislada. Por eso es importante que la función que tenemos dentro de data sea justamente una función que devuelva un objeto. Si en lugar de una función tuviésemos un objeto no seríamos capaces de mantener una referencia aislada para cada componente. De esta forma podemos garantizar que cada componente tiene su propio estado y que cuando se modifica el estado de un componente, por más que sea el mismo tipo de componente, no se va a modificar el estado del otro.

```vue
<div id="app" v-bind:style="{background: '#'+ color}">

    <coin-detail 
    v-on:change-color="updateColor"
    v-bind:coin="btc"
    >  </coin-detail>

    <coin-detail 
    v-on:change-color="updateColor"
    v-bind:coin="btc"
    >  </coin-detail>

    <coin-detail 
    v-on:change-color="updateColor"
    v-bind:coin="btc"
    >  </coin-detail>

  </div>
```

## Slots

-¿ Qué es?
Es una API de distribución de contenido.

- ¿Para qué se utiliza?
  Para inyectar código HTML a un componente hijo
- ¿Cómo se utiliza?
  En el componente hijo, dentro de su template ponemos:

```vue
<slot name="nombreDelSlot"></slot>
```

Mientras que en el HTML, dentro del tag de nuestro componente ponemos:

```vue
<template v-slot="nombreDelSlot"></template>
```

Con el ejemplo que venimos manejando sería:

```vue
<slot name="text"></slot>
<slot name="link"></slot>
<coin-detail :coin="btc" @change-color="updateColor">
	<template v-slot:text>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore rerum optio natus sapiente consequuntur nam possimus soluta deleniti, dolorum minima ut eligendi totam praesentium eum cumque eaque nobis! Odio, sunt!</p>
	</template>
	<template v-slot:link>
		<a href="http://platzi.com">Platzi</a>
	</template>
</coin-detail>
```

## Ciclo de Vida y Hooks

Vue tiene diferentes estados y esto nos permite usar ciertas acciones antes y después de cada estado.

A estas acciones se las conoce como hooks y tienen unos propósitos claros:

- Nos permite conocer como se crea, actualiza y se destruye un componente.
- Podemos saber en que punto se encuentra el componente y poder actuar en consecuencia.

A continuación vamos a ver los hooks que nos ofrece Vue:

1. beforeCreate.
2. created.
3. beforeMount.
4. mounted.
5. beforeUpdate.
6. updated.
7. beforeDestroy.
8. destroyed.

Lifecycle Hooks

Los hooks son diferentes eventos que podemos representar dentro de nuestros componentes a través de funciones, con lo cual podemos disparar código a medida que nuestra componente transite por estas diferentes etapas.

https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram

Según la documentación oficial de Vue, los estados que transita un componente son:

1. beforeCreate

2. created

3. beforeMount

4. mounted

5. beforeUpdate

6. updated

7. beforeDestroy

8. destroyed

   Para su implementación se utilizan como funciones dentro de la configuración de nuestro componente (al nivel de data, methods, etc), son parte del core de VueJS

```js
created(){
console.log('Created...')
},
```

created es bueno para obtener información de una API Rest porque es de lo primero que ocurre dentro del ciclo de vida. En el mounted ya está disponible el DOM, para poder acceder información como el HTML.

diagrama del ciclo de vida de Vue: https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram

hooks que no se mencionaron:

- activated: Se llama cuando se activa un componente que se mantiene vivo.
- deactivated: Se llama cuando se desactiva un componente que se mantiene vivo.
- errorCaptured: Se llama cuando se captura un error de cualquier componente descendiente. El hook recibe tres argumentos: el error, la instancia del componente que desencadenó el error y una cadena que contiene información sobre dónde se capturó el error. El hook puede devolver falso para evitar que el error se propague más.

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram/images/icons/android-icon-192x192.png)The Vue Instance — Vue.js](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)

## Antes de continuar, ¡un repaso!

[Vue Mastery](https://www.vuemastery.com/) 👊😎

![Screen Shot 2020-03-30 at 23.38.38.png](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-03-30%20at%2023.38.38-75c42f78-e29e-44db-b2bf-2050b588fbf8.jpg)

Resumen de el modulo de los componentes:

- Un componente se crea gracias a la función **Vue.component()** esta recibe 2 parámetros, el **nombre del componente**, y el objeto con la **configuración del componente**.
- Propiedades nuevas en los componentes:
  Template => Aquí va todo el html que crea el componente.
  Props => Permite traer información del componente padre.
- La forma de enviar información del componente padre, al componente hijo es a través de props.
- La forma de enviar información del componente hijo, al componente padre es a través de eventos.
- El componente hijo nunca debe modificar las propiedades que le pasa el componente padre.
- Los slots, permiten que un componente padre le inyecte HTML en tiempo real a u componente hijo.
- Y por ultimo que los componentes tienen un ciclo de Vida (Lifecycle), Que nos permite conocer como se crea, actualiza y destruye un componente; y así poder encontrar el punto en el que queremos aplicar una acción

## Ejercicios de práctica

### Práctica Módulo 3

> Armar un componente modal que se muestre al hacer click en un boton del componente padre (root) y que pueda ocultarse/cerrarse con otro boton dentro del componente modal

Hace fork de [este](https://codepen.io/ianaya89/pen/LYYJzor) **CodePen** para obtener la estructura básica del proyecto:

### Consideraciones:

- El componente `modal` debe:
  - Usar una propiedad `title` para poder establecer el titulo del modal.
  - Definir un `slot` para poder establecer el `body` del modal.
  - Tener un boton con una directiva `v-on` a un metodo que emita un evento llamado `close`.
- El componente `root` (Componente Principal) debe:
  - Utilizar el component `modal` y mostrarlo solo cuando la propiedad `showModal` sea `true`.
  - Definir una propiedad `boolean` llamada `showModal` que permite ocultar y mostrar el modal.
  - Definir un método `toggleModal` que permita cambiar el valor de la propiedad `showModal` a su valor contrario (este metodo debe usarse con el boton “Show Modal”).
  - Escuchar el evento `close` del `modal` y ejecutar el metodo `toggleModal` cuando este se emite.
  - Establecer el contenido de `title` del modal usando una propiedad y el contenido del `body`usando slots.

#### [Solución](https://codepen.io/ianaya89/pen/VwwGMOE?)

Buen ejercicio para poner en práctica lo aprendido.

Me quedo asi:
![Selección_001.png](https://static.platzi.com/media/user_upload/Selecci%C3%B3n_001-122e503d-ef3b-425e-9459-5cfb8448537e.jpg)

Mi código:

- HTML

```html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modal With Vue || JCode</title>

    <link rel="stylesheet" href="main.css">
</head>
<body>
    <div id="app">
        <button v-on:click="toggleModal">Show Modal</button>
        
        <!-- primero: para mostrar el modal, segundo: para enviar el titulo al hijo, tercero: el hijo envia el evento al padre, cuarto: el template envia html al hijo -->
        <modal
        v-show="showModal"
        v-bind:modal="modalData"
        v-on:change-valor="toggleModal">
            <template v-slot:body-modal>
                <p>Este es el grandioso modal que hicimos con Vue.js, aqui deberias de poner el contenido.</p>

                <img src="undraw.svg" alt="Image modal"/>
            </template>
        </modal>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="app.js"></script>
</body>
</html>
```

- CSS

```css
#app {
    padding: 20px;
    text-align: center;
}

.modal-mask {
    position: fixed;
    z-index: 9999;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, .5);
    display: flex;
    justify-content: center;
    align-items: center;
    transition: opacity .3s ease;
}
.modal-container {
    width: 300px;
    padding: 20px 30px;
    text-align: left;
    background-color: #fff;
    border-radius: 2px;
    box-shadow: 0 2px 8px rgba(0, 0, 0, .35);
    transition: all .5s ease;
}

.modal-container h3 {
    text-transform: uppercase;
    color: teal;
}
.modal-container .body {
    margin: 20px 0;
    color: dimgrey;
}
.body img {
    width: 100%;
}
.modal-container footer {
    text-align: right;
}
button {
    outline: none;
    cursor: pointer;
    padding: 10px 20px;
    font-weight: bold;
    background-color: dodgerblue;
    border:none;
    border-radius: 5px;
    color: floralwhite;
    transition: transform .3s ease;
}
button:hover {
    background-color: rgb(20, 107, 194);
    transform: scale(1.1);
}
```

- JS

```js
Vue.component('modal', {
    props: ['modal'],

    data() {
        return {
            
        }
    },

    // emitimos el evento al padre
    methods: {
        close() {            
            this.$emit('change-valor')
        },
    },

    template: `
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <h3>{{ modal.title }}</h3>

                    <div class="body">
                        <slot name="body-modal"></slot>
                    </div>

                    <footer>
                        <button v-on:click="close">Cerrar</button>
                    </footer>
                </div>
            </div>
        </div>
    `
})

new Vue({
    el: '#app',

    data() {
        return {
            modalData: {
                title: 'Un Modal Increible', 
            },
            showModal: false,
        }
    },

    methods: {
        toggleModal() {
            this.showModal = !this.showModal
        }
    }
})
```

# 4. Ambiente de Desarrollo

## VS Code + Vetur / Chrome / Firefox + Dev Tools

Process un Objecto global que proporciona información y control sobre la instancia actual de Node.js

El objecto process es una instancia de EvenEmmiter.

La propiedad process.env retorna un objeto con las variables del entorno de usuario.

[Process docs](https://nodejs.org/dist/latest-v8.x/docs/api/process.html#process_process).

[![img](https://www.google.com/s2/favicons?domain=https://nodejs.org/es//static/images/favicons/favicon-32x32.png)Node.js](https://nodejs.org/es/)

[![img](https://www.google.com/s2/favicons?domain=https://code.visualstudio.com//favicon.ico)Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)GitHub - vuejs/vetur: Vue tooling for VS Code.](https://github.com/vuejs/vetur)

[ESLint - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

[![img](https://www.google.com/s2/favicons?domain=https://prettier.io//icon.png)Prettier · Opinionated Code Formatter](https://prettier.io/)

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)GitHub - vuejs/vue-devtools: ⚙️ Browser devtools extension for debugging Vue.js applications.](https://github.com/vuejs/vue-devtools)

## Qué es, cómo usarlo y aplicaciones profesionales con el CLI

**Instalar vue cli**

```sh
npm install -g @vue/cli
```

**Crear proyecto**

```sh
vue create platzi-exchange
```

**Para correr nuestro proyecto:**

```sh
npm run serve
```

[![img](https://www.google.com/s2/favicons?domain=https://cli.vuejs.org//favicon.png)Vue CLI](https://cli.vuejs.org/)

## Single File Components

Son archivos .vue que nos ofrece el Framework para poder trabajar con la lógica, con el CSS o los estilos y con el template, el HTML de un mismo componente. Es decir, en un mismo archivo tendremos el HTML, la parte de lógica con el JS y los estilos con el CSS.

Estos archivos no son soportados por los Browsers, por la parte detrás se tendrá se tendrá todo un Tuling generado por el CLI generando un archivo entendible por el browser a partir del .vue. Es decir tomará todos los archivos vue, convirtiéndolos a JS que sea entendible por el browser.

**Importar componentes:**

Lo que se realiza es importar el componente en dónde se requiere utilizar, y se declara dentro de la propiedad componentes para decirle que está disponible en la propiedad componentes para ser utilizado.

```js
import HelloWorld from "./components/HelloWorld.vue";

export default {
  name: "App",
  components: {
    HelloWorld,
  },
};
```

De ésta forma se importa un componente, se declara dentro de componente y se utiliza así en el HTML:

```vue
<HelloWorld msg="Welcome to Your Vue.js App" />
```

## Funcionalidades, UI y comandos básicos

Son archivos .vue que nos ofrece el framework para poder trabajar con la lógica, con el css y con el HTML dentro de un mismo componente, es decir dentro de un mismo archivo vamos a tener las 3 partes. Estos archivos no son soportados por los browsers por lo que tendremos todo un tooling por detrás configurado por el CLI que se encarga de que generemos un código entendible por el browser a partir de el .vue

**Las librerías no tienen “./” o ruta relativa.**

$mount(’#app’) tiene el mismo propósito que que el “el” en las primeras prácticas.
La función render es una forma de reemplazar la instancia de la app dentro de nuestro componente, es decir, que esa configuración equivale a lo que hacíamos dentro de las apps que hemos hecho.

Cuando corremos npm run serve lo que hace el CLI es usar webpack y otras cosas para generar una app.

Son archivos .vue que nos ofrece el framework para poder trabajar con la lógica, con el CSS y el template de JavaScript dentro del mismo componente.

- El atributo scope nos indica que el estilo se va a aplicar únicamente a este componente.

> **Errores de Linteo**
> Un linter revisa tu código buscando errores y problemas de sintaxis, se asegura también que el código que escribes siga un estilo, esto ayuda a que todo el código escrito en un proyecto se vea como si hubiera sido escrito por la misma persona, y eso lo hace mas legible y mantenible.

**npm run lint.** Detectar errores de linteo
**npm run build**. Generar la aplicación en modo producción. Genera una carpeta dist.
**npm i -g serve**: Instala de manera global el comando serve, mismo que sirve para servir código html y que se muestre en un servidro local

```sh
npm i -g serve #Este nos permite generar un servidor web local
```

**serve -s dist:** sirve el código dentro de la carpeta dist en un servidor local, lo que significa que abre un puerto con todos los archivos estáticos en un host creado en el mismo computador (localhost)

```sh
serve -s dist 
```

![image-20200428071456378.png](https://static.platzi.com/media/user_upload/image-20200428071456378-bb18ba79-cbd0-46c6-8c4f-83c94fc66997.jpg)

**vue ui.**  Genera una aplicación web local, que mediante una interfaz de usuario permite gestionar los proyectos de Vue.

```sh
vue ui
```

![image-20200428072222872.png](https://static.platzi.com/media/user_upload/image-20200428072222872-fd70236d-ab98-4751-89d5-4f15b02b6be3.jpg)

**preset.** A medida que se van creando proyectos con determinada configuración, el cli de vue permite guardar esa configuración para más adelante poder utilizarla al momento de crear un proyecto.



# 5. Platzi Exchange

## Introducción y Setup de Tailwind CSS

Si tienes problemas con la instalación del plugin pueden usar este otro que esta actualizado y también es fácil de configurar:

>  vue-cli-plugin-tailwind

Solo tienen que ejecutar en consola:

```sh
vue add tailwind
```

Y seleccionar opción " full " para agregar toda la configuración de tailwind, luego puedes seguir con la clase con normalidad

[![img](https://www.google.com/s2/favicons?domain=https://tailwindcss.com//favicon-32x32.png)Tailwind CSS - A Utility-First CSS Framework for Rapidly Building Custom Designs](https://tailwindcss.com/)

## Primeros Componentes

**Componente**

```vue
<template>
   <header class="shadow w-screen">
       <nav>
           <nav class="flex items-center justify-between flex-wrap bg-green-400 p-6">
               <div class="flex items-center flex-shrink-0 text-white mr-6">
                   <span class="font-semibold text-xl tracking-tight">PlatziExchange</span>
               </div>
               <div class="hidden sm:block w-full blok flex-grow lg:flex lg:items-center lg:wauto">
                   <div class="text-sm lg:flex-grow"></div>
               </div>
           </nav>
       </nav>
    </header> 
</template>
```

> Nota: Prettier corrige los estilos siempre y cuando tengas configurado en el vscode esta opción, Format On Save : True o dar visto bueno

## Introducción y Setup de Vue Router

`route.js`

```js
import Vue from 'vue';
import Router from 'vue-router';

import Home from '@/views/Home';
import Error from '@/views/Error';
import About from '@/views/About';

Vue.use(Router);

export default new Router({
  mode: 'history',

  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },

    {
      path: '/about',
      name: 'about',
      component: About,
    },

    {
      path: '*',
      name: 'error',
      component: Error,
    },
  ],
});

```

Vue.js 2. El codigo para instalar la version del vue-router compatible.

```sh
npm i vue-router@3.5.3
```

## Introducción a Fetch y API de Coincap

La visualización de números es un poco compleja, así que decidí agregar 2 funciones en el componente de la tabla para darles formato.

------

## Preview

![Screen Shot 2020-11-07 at 3.28.45 PM.png](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-07%20at%203.28.45%20PM-455ab9c5-8537-4799-ab04-7503ab83b602.jpg)

------

<h2>Código</h2>

```javascript
<template>
  <table>
    <thead>
      <tr class="bg-gray-100 border-b-2 border-gray-400">
        <th></th>
        <th>
          <span>Ranking</span>
        </th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Cap. de Mercado</th>
        <th>Variación 24hs</th>
        <td class="hidden sm:block"></td>
      </tr>
    </thead>
    <tbody>
      <tr
        class="border-b border-gray-200 hover:bg-gray-100 hover:bg-orange-100"
        v-for="(asset, idx) in assets"
        :key="`asset-${idx}`"
      >
        <img
          :src="`https://static.coincap.io/assets/icons/${asset.symbol.toLowerCase()}@2x.png`"
          :alt="asset.name"
          style="max-height: 65px"
        >
        <td>
          <b>
            #{{ asset.rank }}
          </b>
        </td>
        <td>{{ asset.name }}</td>
        <td>{{ currencyFormater(asset.priceUsd) }}</td>
        <td>{{ numberFormat(asset.marketCapUsd) }}</td>
        <td>{{ numberFormat(asset.changePercent24Hr) }}</td>
        <td class="hidden sm:block"></td>
      </tr>
    </tbody>
  </table>
</template>

<script>
export default {
  name: 'PxAssetsTable',
  props: {
    assets: {
      type: Array,
      default: () => [],
    },
  },
  methods: {
    currencyFormater(val) {
      return Intl.NumberFormat('en-US', { currency: 'USD', style: 'currency' }).format(val);
    },
    numberFormat(val) {
      if (val) {
        return parseFloat(val).toLocaleString();
      }
      return val;
    },
  },
};
</script>
```

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/static/img/favicon32.7f3da72dcea1.png)Fetch API - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

[![img](https://www.google.com/s2/favicons?domain=https://coincap.io//static/icons/favicon.ico)CoinCap.io | Reliable Cryptocurrency Prices and Market Capitalizations](https://coincap.io/)

[![img](https://www.google.com/s2/favicons?domain=https://coincap.io/favicon.ico)CoinCap API 2.0](https://docs.coincap.io/?version=latest)

[![img](https://www.google.com/s2/favicons?domain=https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Usar_promesas/static/img/favicon32.7f3da72dcea1.png)Usar promesas - JavaScript | MDN](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Usar_promesas)

## Mejorar la UI con filtros

Instlar numerals

```sh
npm i numeral
```

`filter`

```js
import numeral from 'numeral'

const dollarFilter = function(value) {
  if (!value) {
    return '$ 0'
  }
  return numeral(value).format('($ 0.00a)')
}

const percentFilter = function(value) {
  if (!value) {
    return '0%'
  }

  return `${Number(value).toFixed(2)}%`
}

export { dollarFilter, percentFilter };

```

`main.js`

```js
import router from "@/router";
import { dollarFilter, percentFilter } from '@/filters';

Vue.filter('dollar', dollarFilter)
Vue.filter('percent', percentFilter)
```

`pxAssetsTable.vue`

```vue
  <tbody>
      <tr
      v-for="a in assets"
      :key="a.id"
        class="border-b border-gray-200 hover:bg-gray-100 hover:bg-orange-100"
      >
        <td>
          <img 
            class="w-6 h-6"
            :src="`https://static.coincap.io/assets/icons/${a.symbol.toLowerCase()}@2x.png`" 
            :alt="a.name"
          />
        </td>
        <td>
          <b>#{{ a.rank }}</b>
        </td>
        <td>{{ a.name }}</td>
        <td>{{ a.priceUsd | dollar }}</td>
        <td>{{ a.marketCapUsd | dollar }}</td>
        <td :class="a.changePercent24Hr.includes('-') ? 'text-red-600' : 'text-green-600'">
          {{ a.changePercent24Hr | percent }}
          </td>
        <td class="hidden sm:block"></td>
      </tr>
    </tbody>
```

**LOS FILTROS EN VUE 3 YA NO EXISTEN**

Según su [documentación oficial](https://v3.vuejs.org/guide/migration/filters.html#_3-x-update) los filtros dentro de VueJS dejaron de existir, en su lugar reecomiendan usar propiedades computadas o métodos.

Aquí les dejo el código para que puedan simular el uso de filtros siguiendo la documentación de Vue, el archivo main.js ya no se toca, ahí ya no hacemos nada, pero en el componente PxAssetsTable.vue hay que agregar importar dollarFilter y hay que agregar el método, que en este caso lo llame dollar para simular el filtro de la clase, el cual va a devolver lo que la función dollarFilter devuelva:

```js
import { dollarFilter } from "@/filters";

export default {
  name: "PxAssetsTable",

  props: {
    assets: {
      type: Array,
      default: () => []
    }
  },

  methods: {

    dollar(value) {
      return dollarFilter(value);
    }

  }

};
```

Y simplemente en el template lo usamos como un método normal:

```vue
<td>{{ dollar(a.priceUsd) }}</td>
```

## Rutas dinámicas

async y await

```js
//CoinDetail.vue
 created() {
    this.getCoin();
  },

  methods: {
    async getCoin() {
      const id = this.$route.params.id;
      let [asset, history] = await Promise.all([
        getAsset(id),
        getAssetHistory(id),
      ]);
      this.asset = asset;
      this.history = history;
    },
  },
};

//api.js
const url = "https://api.coincap.io/v2";

const getAssets = async () => {
  try {
    const req = await fetch(`${url}/assets?limit=20`);
    const res = await req.json();
    return await res.data;
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
};

const getAsset = async (coin) => {
  try {
    const req = await fetch(`${url}/assets/${coin}`);
    const res = await req.json();
    return await res.data;
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
};

const getAssetHistory = async (coin) => {
  try {
    const now = new Date();
    const end = now.getTime();
    now.setDate(now.getDate() - 1);
    const start = now.getTime();
    const req = await fetch(
      `${url}/assets/${coin}/history?interval=h1&start=${start}&end=${end}`
    );
    const res = await req.json();
    return await res.data;
    
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

export { getAssets, getAsset, getAssetHistory };
```

### Date: método getTime()

El método **getTime()** devuelve el valor numérico correspondiente a la hora para la fecha especificada según la hora universal.

Puede utilizar este método para ayudar a asignar una fecha y hora a otro objeto Date. Este método es funcionalmente equivalente al metodo valueOf() (en-US).
Ejemplo:

```js
dateObj.getTime()
```

### Valor Devuelto

El valor devuelto por el método getTime() es un número de milisegundos **desde el 1 de enero de 1970 00:00:00 UTC.**
[***Documentación:\*** ](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Date/getTime)

## Navegación Programática

`PxButton.vue`

```vue
<template>
  <button
    @click="buttonClick"
    class="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-2 border border-green-500 hover:border-transparent rounded"
  >
  <slot></slot>
  </button>
</template>


<script>
export default {
  name: 'PxButton',

  methods: {
    buttonClick() {
      this.$emit('custom-click')
    }
  }
}
</script>
```

## Utilizar componentes de terceros

Instalación:

```sh
npm i -S @saeris/vue-spinners vue-chartkick
```

**CoinDetail** separé el array que mandamos a una computed nombrado chartaData:

```html
<line-chart
  class="my-10"
  :colors="['orange']"
  :min="min"
  :max="max"
  :data="chartData"
/>	
```

Y la propiedad computada va:

```js
chartData() {
  const data = []
  this.history.map(h => {
    data.push([h.date, parseFloat(h.priceUsd).toFixed(2)])
  })
  return data
}
```

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[Vue Spinners](https://github.com/Saeris/vue-spinners)

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)GitHub - greyby/vue-spinner: vue spinners](https://github.com/greyby/vue-spinner)

[![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)GitHub - ankane/vue-chartkick: Create beautiful JavaScript charts with one line of Vue](https://github.com/ankane/vue-chartkick)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - Saeris/vue-spinners: 💫 A collection of loading spinner components for Vuejs](https://github.com/Saeris/vue-spinners)

## Problemas de Reactividad

tabla vacía para que no se líen buscandola:

```vue
<h3 class="text-xl my-10">Mejores Ofertas de Cambio</h3>
      <table>
        <tr class="border-b">
          <td>
            <b></b>
          </td>
          <td></td>
          <td></td>
          <td>
            <px-button />
            <a class="hover:underline text-green-600" target="_blanck"></a>
          </td>
        </tr>
      </table>
```

En Vue 3 la función this.$set ya no existe, en Vue 3 el sistema de reactividad está basado en “proxies” así que ahora Vue es capaz de detectar la adicion de nuevas propiedades a los objetos y arrays, por lo que hacerlo de la [primera forma funcionará sin problemas](https://forum.vuejs.org/t/uncaught-in-promise-typeerror-this-set-is-not-a-function-for-new-project-on-vue3/104698).

 async/await, try catch para preveer problemas si no funciona el request:

```js
const getMarkets = async coin => {
  try {
    let res = await fetch(`${baseUrl}/assets/${coin}/markets?limit=5`);
    res = await res.json();
    return res.data;
  } catch (error) {
    console.error("Error en el request");
  }
};
const getExchange = async id => {
  try {
    let res = await fetch(`${baseUrl}/exchanges/${id}`);
    res = await res.json();
    return res.data;
  } catch (error) {
    console.error("Error en el request");
  }
```

## Mejorar proyecto: filtros y ordenar

HTML:

```vue
<input
            class="bg-gray-100 focus:outline-none border-b border-gray-400 py-2 px-4 block w-full appearance-none leading-normal"
            id="filter"
            placeholder="Buscar..."
            type="text"
            v-model="filter"
          />
```

Podria evitar el “altOrder” retornando en caso de no cumplir la condicion de a.rank > b.rank, el opuesto de sortOrder

```js
.sort((a, b) => {
          if (parseInt(a.rank) > parseInt(b.rank)) {
            return this.sortOrder;
          }

          return this.sortOrder * -1;
        });
```

![arranging.png](https://static.platzi.com/media/user_upload/arranging-68392751-b7a2-4ec8-b8c0-33787a460d3f.jpg)

```vue
<template>
  <table>
    <thead>
      <tr class="bg-gray-100 border-b-2 border-gray-400">
        <th></th>
        <th :class="{up: this.sortOrder === 1, down: this.sortOrder === -1}">
          <span 
          class="underline cursor-pointer"
          @click="changeSortOrder">Ranking</span>
        </th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Cap. de Mercado</th>
        <th>Variación 24hs</th>
        <td class="hidden sm:block">
          <input
            class="bg-gray-100 focus:outline-none border-b border-gray-400 py-2 px-4 block w-full appearance-none leading-normal"
            id="filter"
            placeholder="Buscar..."
            type="text"
            v-model="filter"
          />
        </td>
      </tr>
    </thead>
    <tbody>
      <tr
        v-for="a in filteredAssets"
        :key="a.id"
        class="border-b border-gray-200 hover:bg-gray-100 hover:bg-orange-100"
      >
        <td>
          <img
            class="w-6 h-6"
            :src="`https://static.coincap.io/assets/icons/${a.symbol.toLowerCase()}@2x.png`"
            :alt="a.name"
          />
        </td>
        <td>
          <b># {{ a.rank }}</b>
        </td>
        <td>
          <router-link 
          class="hover:underline text-green-600"
          :to="{name: 'coin-detail', params: { id: a.id }}">
            {{ a.name }}
          </router-link>
          <small class="ml-1 text-gray-600">{{a.symbol}}</small>
        </td>
        <td>{{ dollarFilter(a.priceUsd) }}</td>
        <td>{{ dollarFilter(a.marketCapUsd) }}</td>
        <td
          :class="
            a.changePercent24Hr.includes('-')
              ? 'text-red-600'
              : 'text-green-600'
          "
        >
          {{ percentFilter(a.changePercent24Hr) }}
        </td>
        <td class="hidden sm:block">
          <px-button @click="goToCoin(a.id)"><span>Detalle</span></px-button>
        </td>
      </tr>
    </tbody>
  </table>
</template>

<script>
import { dollarFilter, percentFilter } from '../filter'
import PxButton from './PxButton.vue';

export default {
  name: 'PxAssetsTable',
  components: { PxButton },

  data(){
    return {
      filter: '',
      sortOrder: 1
    }
  },

  props: {
    assets: {
      type: Array,
      default: () => [],
    },
  },

  computed: {
    filteredAssets(){
      return this.assets.filter(
        a => 
        a.symbol.toLowerCase().includes(this.filter.toLowerCase()) || 
        a.name.toLowerCase().includes(this.filter.toLowerCase())
      )
      .sort((a,b)=>{
        if(parseInt(a.rank) > parseInt(b.rank)){
          return this.sortOrder
        }
        return this.sortOrder * -1;
      })
    }
  },

  methods: {
    goToCoin(id){
      this.$router.push({name: 'coin-detail', params:{id}})
    },

    changeSortOrder(){
      this.sortOrder = this.sortOrder === 1 ? -1 : 1
    }
  },

  setup() {
    return {
      dollarFilter,
      percentFilter,
    }
  },
}
</script>

<style scoped>
.up::before {
  content: '👆';
}

.down::before {
  content: '👇';
}

td {
  padding: 20px 0px;
  font-size: 0.6rem;
  text-align: center;
}

th {
  padding: 5px;
  font-size: 0.6rem;
}

@media (min-width: 640px) {
  td,
  th {
    padding: 20px;
    font-size: 1rem;
  }

  th {
    padding: 12px;
  }
}
</style>
```

## Mejorar proyecto: links y conversión

HTML:

```vue
<router-link class="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4" ></router-link>
```

Data:

```js
links: [
        {
          title: 'BTC',
          to: { name: 'coin-detail', params: { id: 'bitcoin' } }
        },
        {
          title: 'ETH',
          to: { name: 'coin-detail', params: { id: 'ethereum' } }
        },
        {
          title: 'XRP',
          to: { name: 'coin-detail', params: { id: 'ripple' } }
        }
      ]
```

> La API cambió el nombre de ripple por xrp, aspi que cuando seleccionen XRP no les va a servir, lo unico que tienen que hacer es cambiar dentro de links en el objeto de xrp, en donde dice params, hay que cambiar “ripple” por “xrp” y funcionará de nuevo

Al parecer cambiaron el id del ripple en la API y da un error cuando intentas traer la información.
Para solucionarlo solo cambien el id por XRP en el array de links

```js
{
          title: "XRP",
          to: { name: "coin-detail", params: { id: "xrp" } },
}
```

# 6. ¡A producción!

## Despliegue de la app a Netlify

[![img](https://www.google.com/s2/favicons?domain=https://www.netlify.com//img/global/favicon/favicon-32x32.png)Netlify: All-in-one platform for automating modern web projects.](https://www.netlify.com/)