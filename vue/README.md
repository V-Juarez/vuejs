<h1>Vue</h1>

- [Vue](#vue)
  - [Configuration Vue](#configuration-vue)
  - [Contacto vue](#contacto-vue)
- [Renderizado Declarativo](#renderizado-declarativo)
  - [Interpolacion de datos](#interpolacion-de-datos)
  - [Atributos reactivos](#atributos-reactivos)
- [Input de usuario](#input-de-usuario)
  - [Eventos de Usuario](#eventos-de-usuario)
  - [Inputs reactivos](#inputs-reactivos)
- [Reactividad](#reactividad)
  - [Propiedades computadas](#propiedades-computadas)
  - [Watchers](#watchers)
  - [Estilos reactivos](#estilos-reactivos)
- [Listas y condicionales](#listas-y-condicionales)
  - [Condicionales](#condicionales)
  - [Listas](#listas)
- [Componentes personalizados](#componentes-personalizados)
  - [Componentes](#componentes)
  - [Slots](#slots)
- [Comunicación entre componentes](#comunicación-entre-componentes)
  - [Comunicación de componente padre a hijo](#comunicación-de-componente-padre-a-hijo)
  - [Comunicación de componente hijo a padre](#comunicación-de-componente-hijo-a-padre)
  - [Custom v-model](#custom-v-model)
  - [Comunicación con componentes profundos](#comunicación-con-componentes-profundos)
- [Componentes en el Virtual DOM](#componentes-en-el-virtual-dom)
  - [Instancias de componentes](#instancias-de-componentes)
- [Cierre del curso](#cierre-del-curso)
  - [Vue progresivo](#vue-progresivo)
  - [Esto es solo el comienzo](#esto-es-solo-el-comienzo)

# Vue
## Configuration Vue 

`html`

```vue
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vue</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <script src="https://unpkg.com/vue@next"></script>
  </body>
</html>

```

VueTools - extension web

[![img](https://www.google.com/s2/favicons?domain=https://cli.vuejs.org//favicon.png)Vue CLI](https://cli.vuejs.org/)

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org//logo.svg)Vue.js - The Progressive JavaScript Framework | Vue.js](https://vuejs.org/)

## Contacto vue

```vue
  <body>
    <script src="https://unpkg.com/vue@next"></script>
    <div id="app">
      {{ text }}
    </div>

    <script>
      const mv = Vue.createApp({
        data() {
          return {
            text: "Hola Vue"
          };
        }
      }).mount('#app')
      console.log(mv)
    </script>
  </body>
```

# Renderizado Declarativo

## Interpolacion de datos

template

```vue

    <script>
      const mv = Vue.createApp({
        data() {
          return {
            text: "Hola Rails"
          };
        },
          // template: `<div>{{ text }}</div>`
          template: `<div v-text="text"></div>`
		   // template: `<div v-text="text"></div>`
          // template: `<div v-once v-text="text"></div>`
          template: `<div v-html="text"></div>`

      }).mount('#app')
    </script>

```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/template-syntax.html#raw-html/logo.svg)Template Syntax | Vue.js](https://vuejs.org/guide/essentials/template-syntax.html#raw-html)

## Atributos reactivos

```vue
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vue</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <script src="https://unpkg.com/vue@next"></script>
    <div id="app">
    </div>

    <script>
      const mv = Vue.createApp({
        data() {
          return {
            attr: "src",
            // attr: "alt",
            img: "https://www.xtrafondos.com/wallpapers/man-new-york-3113.jpg"
          };
        },
          // template: `<div v-html="text"></div>`
          // template: `<img v-bind:src="img" v-bind:alt="img" />`
          // template: `<img v-bind:["src"]src="img" />`
          template: `<img v-bind:[attr]="img" />`
      }).mount('#app')
    </script>
  </body>
</html>
```

# Input de usuario

## Eventos de Usuario

```vue
const vm = Vue.createApp({
        data() {
          return {
            count: 1,
          };
        },
        methods: {
          changingCounter(value) {
            this.count += value;
          },
        },
        template: `<div>
        <div>{{count}}</div>
        <button v-on:click="changingCounter(1)" type="button">+1</button>
        <button v-on:click="changingCounter(-1)" type="button">-1</button>
          </div>
          `,
      }).mount('#app');
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/api/built-in-directives.html#v-on/logo.svg)Built-in Directives | Vue.js](https://vuejs.org/api/built-in-directives.html#v-on)

## Inputs reactivos

Atributos -> `:`

Eventos -> `@`

```vue
<body>
    <script src="https://unpkg.com/vue@3"></script>
    <div id="app">
        <h1>{{count}}</h1>
        <button @click="incrementar">Incrementar</button>
        <button @click="disminuir">Disminuir</button>
        <div>
            <h2>{{text}}</h2>
            <input v-model="text">
        </div>
    </div>
    
    <script>
        const vm = Vue.createApp({
            data() {
                return {
                    count: 0,
                    text: 'Hola vue!!'
                }
            },

            methods: {
                incrementar(){
                    this.count++;
                },

                disminuir(){
                    this.count--;
                },
                input(e){
                    this.text = e.target.value
                }
            },     
        }).mount('#app')
        //console.log(vm);
    </script>
</body>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/forms.html/logo.svg)Form Input Bindings | Vue.js](https://vuejs.org/guide/essentials/forms.html)

# Reactividad

## Propiedades computadas

Una diferencia entre una computada y un método es que la computada no necesita ser llamada para que escuche los cambios. El método como dice la profe, puede retornar el valor deseado, pero siempre hay que hacer un llamado al mismo cuando queramos tenerlos en la vista.

> ## 😮
>
> ## Methods == Logica de eventos
>
> ## Computed == Logica de data en general

```vue
 <script>
        const vm = Vue.createApp({
            data() {
                return {
                    firstName: "Checo",
                    lastName: "Perez",
                    now: new Date()
                };
            },
            created() {
                var self = this;
                window.setInterval(() => self.updateDateTime(), 1000);
            },
            methods: {
                updateDateTime() {
                    this.now = new Date();
                }
            },
            computed: {
                fullName() {
                    return this.firstName + " " + this.lastName;
                },
                today() {
                    return this.now.toLocaleString();
                }
            },
            template:
                `
                <div> {{ fullName }} </div>
                <input v-model="firstName" />
                <input v-model="lastName" />
                <div>{{ today }}</div>
                `
        }).mount("#app");
        console.log(vm);
    </script>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/computed.html/logo.svg)Computed Properties | Vue.js](https://vuejs.org/guide/essentials/computed.html)

## Watchers

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <p>{{ text }}</p>
        <button @click="open=!open">{{ label }}</button>
    </div>
    
    <script src="https://unpkg.com/vue@next"></script>

    <script>
        const vm = Vue.createApp( {
            data() {
                return {
                    text: "Puerta cerrada",
                    open: false,
                }
            },
            computed: {
                label() {
                    return this.open ? "Cerrar" : "Abrir";
                } 
            },
            watch: {
                open(value) {
                    value ? this.text="Puerta abierta" : this.text="Puerta cerrada";
                }
            }

        }).mount("#app");
    </script>

</body>
</html>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/watchers.html/logo.svg)Watchers | Vue.js](https://vuejs.org/guide/essentials/watchers.html)

## Estilos reactivos

```vue
 template: `
              <div class="container" :class="{'open': open, 'closed': !open }">
                <h2>{{ text }}</h2>
                <button @click="open = !open">{{ label }}</button>
              </div>
            `
```



```vue
const vm = Vue.createApp({
            data(){
                return {
                    text: "Puerta cerrada",
                    open: false,
                    styles: {
                        backgroundColor: "#eca1a6",
                    },
                };

               
            },

            methods: {

                toggleDoor(){
                    this.open = !this.open;

                    this.open ? this.styles.backgroundColor = "#b5e7a0" : this.styles.backgroundColor = "#eca1a6"
                }

            },

            watch: {
                open(value){
                    if (value){
                        this.text = "Puerta abierta";
                    }
                    else{
                        this.text = "Puerta cerrada";
                        
                    }
                }
            },

            template: `
                <div class="container" :style="styles">
                    <h2>{{text}}</h2>
                    <button @click="toggleDoor">{{open ? "Cerrar" : "Abrir"}}</button> 
                </div>
                
            `
        }).mount("#app");
```

`styles`

```css
<style>
        html, body {
            height: 100vh;
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }
        #app, .container {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            width: 100%;
            height: 100%;
        }
        button {
            margin-top: 24px;
            border: none;
            background-color: white;
            padding: 8px 24px;
            border-radius: 12px;
        }
        .closed {
            background-color: #eca1a6;
        }
        .open {
            background-color: #b5e7a0;
        }
    </style>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/class-and-style.html/logo.svg)Class and Style Bindings | Vue.js](https://vuejs.org/guide/essentials/class-and-style.html)

# Listas y condicionales

## Condicionales

> Una forma de aprovechar los condicionales puede ser cuando en tiempo real escribimos en un input con validación de entrada, por ejemplo, en campo un campo de email, si el email es erróneo nos genera un html con estilos debajo del input de que el email no es correcto.

![vue1.png](https://static.platzi.com/media/user_upload/vue1-d14f9cd9-ee08-486b-bee8-f113fc0b8f4a.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/conditional.html/logo.svg)Conditional Rendering | Vue.js](https://vuejs.org/guide/essentials/conditional.html)

## Listas

Agrego mi solucion al reto, se agrego la funcion eliminar y agregar
[Link de github pages](https://darynka-tapia.github.io/frontend-architecture/vue__tweeter-clone/)
[Link del repositorio](https://github.com/Darynka-Tapia/frontend-architecture/tree/master/vue__tweeter-clone)

![delete.PNG](https://static.platzi.com/media/user_upload/delete-2ca24107-3d4b-4aca-a794-103e6f863838.jpg)
![added.PNG](https://static.platzi.com/media/user_upload/added-01a213d5-d54b-43c6-9c73-65deb8146aaf.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/list.html/logo.svg)List Rendering | Vue.js](https://vuejs.org/guide/essentials/list.html)

# Componentes personalizados

## Componentes

```vue
// Agregando componente, el nombre puede ser en kebab-case o PascalCase
app.component('item-post', {
    // Todos los atributos que debo usar en el componente
    props: ['title', 'desc'],
    // Agregando el template del componente
    template: `
    <div class="item">
        <h3>{{ title }}</h3>
        <p>{{ desc }}</p>
    </div>
    `,
    // Si lo requiere también podemos agregar los propios methods, computed, watch entre otros
});
```

Y usando el compoente de esta manera:

```vue
<div class="list">
    <item-post
        v-for="(item, i) in posts"
        :key="i"
        :title="item.title"
        :desc="item.desc"
    />
</div>
```

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <p>{{ text }}</p>
        <button @click="open=!open">{{ label }}</button>
    </div>
    
    <script src="https://unpkg.com/vue@next"></script>

    <script>
        const app = Vue.createApp( {
            data() {
              return {
                text: "Puerta cerrada",
                open: false,
                username: "",
                posts: [
                  {
                    title: "Titulo 1",
                    description: "lorem ipsum..."
                  },
                  {
                    title: "Titulo 2",
                    description: "lorem ipsum..."
                  },
                  {
                    title: "Titulo 3",
                    description: "lorem ipsum..."
                  },
                  {
                    title: "Titulo 4",
                    description: "lorem ipsum..."
                  }
                ]
              };
            },
            watch: {
              open(value) {
                if (value) {
                  this.text = "Cierra sesion";
                } else {
                  this.text = "Accede a tu cuenta";
                  this.username = ""
                }
              }
            },
            computed: {
              label() {
                return this.open ? "Salier" : "Acceder";
              }, 
              styles() {
                return this.open ? ['open'] : ['closed'];
              }
            },
            template: `
              <div class="container" :class="styles">
                <h2>{{ text }}</h2>
                <div v-if="open">
                  <p>Hola, {{ username }}</p>
                  <div class="list">
                    <item 
                      v-for="(item, i) in posts"
                      :key="i"
                      :post="item"
                    />
                  </div>
                </div>
                <div v-else>
                  <div>Username</div>
                  <input type="text" v-model="username"></input>
                </div>

                <button @click="open = !open">
                  <div v-if="!open">Acceder</div>
                  <div v-else>Salir</div>
                </button>
              </div>
            `
        })
        
        app.component("item", {
          props: ["post"],
          template:
          `
          <div class="item">
            <div class="title">{{ post.title }}</div>
            <p>{{ post.description }}</p>
          </div>
          `
        });
        const vm = app.mount("#app");
    </script>
    <style>
      html, body {
          height: 100vh;
          margin: 0;
          font-family: Arial, Helvetica, sans-serif;
      }
      #app, .container {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
          width: 100%;
          height: 100%;
      }
      button {
          margin-top: 24px;
          border: none;
          background-color: white;
          padding: 8px 24px;
          border-radius: 12px;
      }
      .closed {
          background-color: #eca1a6;
      }
      .open {
          background-color: #b5e7a0;
      }
      .list {
        display: flex;
        flex-direction: column;
      }
      .item {
        border: 1px solid black;
      }

      .title {
        font-weight: bold;
        font-size: 1.2rem;
      }
  </style>
</body>
</html>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/essentials/component-basics.html/logo.svg)Components Basics | Vue.js](https://vuejs.org/guide/essentials/component-basics.html)

## Slots

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <script src="https://unpkg.com/vue@next"></script>
  <div id="app"></div>
  
  <script>
    const app = Vue.createApp({
      template: `
        <div>
          <v-header>
            <template v-slot:header>
              <h2>Titulo Hola Mundo</h2>
            </template>
            <template v-slot:content>
              <span>🌎 </span>
              <p>Beautiful Earth</p>
            </template>
          </v-header
          <v-layout>
            <span>Lorem ipsum dolor 🌽 </span>
          </v-layout>
        </div>
        `
      })
      
      app.component("v-header", {
        template: `
          <header>
            <slot name="header"></slot>  
          </header>
          <div>
            <slot name="content"></slot>
          </div>
        `
      });

      app.component("v-layout", {
        template: `
          <div>
            <slot></slot>
          </div>
        `
      });

    const vm = app.mount("#app");
  </script>
</body>
</html>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/components/slots.html/logo.svg)Slots | Vue.js](https://vuejs.org/guide/components/slots.html)

# Comunicación entre componentes

## Comunicación de componente padre a hijo

**Si no te aparece el texto por defecto en las props**

```vue
text: {
    default: "Texto vacio"
}
```

agregar esto

```vue
items:["unos","dos","tres",,]
```

**Profe se le olvidó explicar esto -> **

```vue
items:["unos","dos","tres",,]
```

Se agregó dos comas al final del array

creo que de alguna manera tienen que estar enlazados para que puede funcionar el default text.

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <script src="https://unpkg.com/vue@next"></script>
  <div id="app"></div>

  <script>
    const app = Vue.createApp({
      data() {
        return {
          items: ["Uno", "Dos", "Tres", "Cuatro",,]
        }
      },
      template:`
        <ul>
          <v-item v-for="item in items" v-bind:text="item"></v-item>
        </ul>
      `
    });

    app.component("v-item", {
      props: {
        text: {
          default: "Texto Vacio"
        }
      },
      template: `<li>{{ text }}</li>`
    });

    const vm = app.mount("#app");
  </script>
</body>
</html>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/components/props.html/logo.svg)Props | Vue.js](https://vuejs.org/guide/components/props.html)



## Comunicación de componente hijo a padre

Para comunicar que paso dentro del componente hijo al padre, se lo hace saber mediante el disparo de eventos, los cuales son personalizados por nosotros

1. Se escucha un evento en el componente hijo, el cual termina por disparar un método.
2. Dicho método dispara otro evento, pero esta vez sera escuchado a nivel jerarquico del componente padre.
3. Ese evento que escucho el padre procede a disparar mas funcionalidades ya se si es simple o no

```vue
// Dentro del componente padre
<v-component v-on:eventoProvenienteHijo="metodo">
//  Dentro del componente hijo
<button
     v-on:click="metodoQueDIsparaEvento">
	Delete
</button>


// Methods del hijo
metodoQueDisparaEvento () {
	this.$emit("eventoProvenienteDelHijo", dataOpcional)
} 
```

En el **componente hijo** también se puede ejecutar el **emit** directamente en el evento, para evitar crear la función **rm**:

```vue
template: `
	<li @click="$emit('remove')">
		{{ text }}
	</li>`
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/components/events.html/logo.svg)Component Events | Vue.js](https://vuejs.org/guide/components/events.html)

## Custom v-model

En los caso en que el padre necesite saber el estado de la alguna data del su componente hijo, se tiene que hacer la vinculación con v-model.

Este v-model es personalizado, pues el nombre de este será igual al nombre del prop ubicado en props.

En el caso de los inputs se necesita hacer uso del v-bind de value, asi como de un metodo que dispare el evento “update:propName”, junto con la data para que se logre esta reactividad que estara presente tanto en la data del padre como en las props del hijo.

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <script src="https://unpkg.com/vue@next"></script>
  <div id="app"></div>
  <script>
    const app = Vue.createApp({
      data() {
          return {
            text: "Hola Vue"
          } ;
      },
      template: `
        <div>
          <p>{{ text }}</p>
          <v-input 
            v-model:value="text" 
          />
        </div>
      `
    });

    app.component("v-input", {
      props: {
        value: String
      },
      methods: {
        input(e) {
          this.$emit("update:value", e.target.value)
          console.log(e.target.value)
        }
      },
      template: `
        <input 
          type="text" 
          v-bind:value="value" 
          v-on:input="input" 
        />`
    });
    
    const vm = app.mount("#app")
  </script>
</body>
</html>
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/components/events.html#usage-with-v-model/logo.svg)Component Events | Vue.js](https://vuejs.org/guide/components/events.html#usage-with-v-model)

## Comunicación con componentes profundos

```vue
app.component("tercero", {
        //inject: ["otroTexto"],
        inject: {
          textNew: {
            from: "otroTexto",
          },
        },
        template: `
                <div>{{textNew}}</div>
            `,
      });
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/guide/components/provide-inject.html/logo.svg)Provide / Inject | Vue.js](https://vuejs.org/guide/components/provide-inject.html)

# Componentes en el Virtual DOM

## Instancias de componentes

```vue
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <script src="https://unpkg.com/vue@next"></script>
  <div id="app"></div>

  <script>
    const app = Vue.createApp({
      data() {
        return {
          text: "Hola Diana"
        };
      },
      template: `
        <v-button ref="elemento" :label="text"></v-button>
      `
    });


    app.component("v-button", {
      props: ["label"],
      template: `<button>{{ label }}</button>`
    });

    const vm = app.mount("#app")
    // console.log(vm.$root)
    // console.log(vm.$el)
    // console.log(vm.$refs.elemento)
    console.log(vm.$refs.props)
  </script>
</body>
</html> 
```

[![img](https://www.google.com/s2/favicons?domain=https://vuejs.org/api/component-instance.html/logo.svg)Component Instance | Vue.js](https://vuejs.org/api/component-instance.html)

# Cierre del curso

## Vue progresivo

La opción de *instant prototyping* ya no aparece en la documentación. Para poder continuar, use el comando

```shell
vue create hello-world
```

Que me crea un nuevo proyecto con “todo incluido” y tan solo borrando lo que está en la carpeta `src/components` y modificando por el código que realizó la profe Diana en `src/App.vue` obtuve el mismo resultado.

```shell
npm install -g @vue/cli @vue/cli-service-global
```

Claro está, para que les funcione también el servidor, deben decirle `npm run serve`

![img](https://www.google.com/s2/favicons?domain=https://cli.vuejs.org//favicon.png)[Style Guide](https://vuejs.org/style-guide/)

[![img](https://www.google.com/s2/favicons?domain=https://cli.vuejs.org//favicon.png)Vue CLI](https://cli.vuejs.org/)

## 

